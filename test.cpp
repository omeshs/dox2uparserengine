// #include <fstream>
// #include <iostream>
// #include <string>
// #include <iostream>
// //#include <filesystem>
// #include <chrono>
// #include <leptonica/allheaders.h>
// #include <tesseract/baseapi.h>
// #include <opencv2/opencv.hpp>

// bool is_bold,is_italic,is_underlined,is_monospace,is_serif,is_smallcaps;
// int pointsize,font_id;

// int main(int argc , char**  argv) {
//   std::string imagePath = "../images";
//   cv::Mat image;
//    cv::Mat readImage= imread(argv[1],cv::IMREAD_COLOR);

//   tesseract::TessBaseAPI *api = new tesseract::TessBaseAPI();
//   //api->Init("/home/omesh/Core_Solucomm/dox2u_Root_L1/SDK_OPENSOURCE/tesseract/tessdata/", "eng", tesseract::OEM_LSTM_ONLY);
//   api->Init(NULL, "eng");
//   api->SetPageSegMode(tesseract::PSM_AUTO);
//   api->SetVariable("debug_file", "tesseract.log");
//   api->SetVariable("textord_debug_printable", "1");
//   api->SetVariable("textord_tablefind_recognize_tables", "1");
//   api-> SetVariable("textord_tabfind_find_tables", "true");
//   api-> SetVariable("textord_tablefind_recognize_tables", "true");
//   api->SetVariable("tessedit_debug_fonts","1");

//   //api-> SetVariable("textord_show_tables", "true");

// //  for (const auto &fn : std::filesystem::directory_iterator(path)) {
//     auto start = std::chrono::steady_clock::now();
// //    auto filepath = fn.path();
// //    std::cout << "Detecting text in " << filepath << std::endl;

//     image = cv::imread(argv[1], 1);

//     api->SetImage(image.data, image.cols, image.rows, 3, image.step);
//     std::string outText = api->GetUTF8Text();
//     //std::cout << outText << std::endl;
	
	
// 	api->Recognize(NULL);
//   tesseract::ResultIterator* ri = api->GetIterator();
//   //NOTE : USE TESSERACT PSM SINGLE COLUMN AND RIL PARA AND THEN CONDITION IS TABLE ONLY TO DETECT BORDER LESS TABLE , ONCE YOU HAVE BLOCK HINT THEN use OPENcv    	computation
//   //	Use PSM_AUTO RIL TEXTLINE. for key value

 
//   tesseract::PageIteratorLevel level = tesseract::RIL_WORD; //RIL_BLOCK detext table completly and image for PT _IMAGE and table for Pt _TABLE
//   if (ri != 0) {
//     do {
//       const char* word = ri->GetUTF8Text(level);
//       float conf = ri->Confidence(level);
//       int x1, y1, x2, y2;
//       ri->BoundingBox(level, &x1, &y1, &x2, &y2);
      
//       //if( ri->BlockType() == PT_FLOWING_IMAGE) // image detection
//       // wihtout if  paragraph
//       //PT_PULLOUT_TEXT text on corners
//       // PT_HEADING_TEXT main heading in images , can be helful in invoice or other forms.
//        //if( ri->BlockType() == PT_VERT_LINE  || ri->BlockType() == PT_HORZ_LINE  || ri->BlockType() == PT_TABLE) //tbale detection
//       {
//         int x1, y1, x2, y2;
//         ri->WordFontAttributes(&is_bold,&is_italic,&is_underlined,&is_monospace,&is_serif,&is_smallcaps,&pointsize,&font_id);
//         ri->BoundingBox(level, &x1, &y1, &x2, &y2);

//         const char* word = ri->GetUTF8Text(level);
//         printf("\ntable BoundingBox: %d,%d,%d,%d;\n", x1, y1, x2, y2);
//         cv::rectangle(readImage,cv::Point(x1,y1),cv::Point(x2,y2),cv::Scalar(0,255,0),3,8,0);

//         std::cout<<"\n"<<word<<"\n"<<"\n\nIS_BOLD:   "<<is_bold<<"\n\nIS_ITALIC:   "<<is_italic<<"\n\nIS_underline:   "<<is_underlined<<"\n\nIS_MONOSPACE:   "<<is_monospace<<"\n\nIS_serif:   "<<is_serif<<"\n\nIS_smallCaps:   "<<is_smallcaps<<"\n\nIS_pointSize:   "<<pointsize<<"\n\nfontID:   "<<font_id;

//         std::cout<<"\n\n@@@@@@@@@@@@@@@@@           "<<ri->WordRecognitionLanguage();
//       }
//       //printf("word: '%s';  \tconf: %.2f; BoundingBox: %d,%d,%d,%d;\n",
//       //         word, conf, x1, y1, x2, y2);
//       delete[] word;
//     } while (ri->Next(level));
//   }

//       imwrite("test.jpg",readImage);
//     auto end = std::chrono::steady_clock::now();
//     std::chrono::duration<double, std::milli> diff = end - start;
//     std::cout << "Computation time: " << diff.count() << "ms" << std::endl;
//  // }
//   api->End();
// }
#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>
#include <iostream>


bool is_bold,is_italic,is_underlined,is_monospace,is_serif,is_smallcaps;
int pointsize,font_id;


int main(int arg, char** argv)
{
    char *outText;

    tesseract::TessBaseAPI *api = new tesseract::TessBaseAPI();
// Initialize tesseract-ocr with English, without specifying tessdata path
    if (api->Init(NULL, "eng")) {
        fprintf(stderr, "Could not initialize tesseract.\n");
        exit(1);
    }

    api->SetVariable("tessedit_debug_fonts","1");

// Open input image with leptonica library
    Pix *image = pixRead(argv[1]);
    api->SetImage(image);
    api->SetVariable("lstm_choice_mode", "2");
// // Get HOCR result
     outText = api->GetHOCRText(0);
// //    printf("HOCR alternative symbol choices  per character :\n%s", outText);


      tesseract::ResultIterator* ri = api->GetIterator();
  //NOTE : USE TESSERACT PSM SINGLE COLUMN AND RIL PARA AND THEN CONDITION IS TABLE ONLY TO DETECT BORDER LESS TABLE , ONCE YOU HAVE BLOCK HINT THEN use OPENcv    	computation
  //	Use PSM_AUTO RIL TEXTLINE. for key value

 
  tesseract::PageIteratorLevel level = tesseract::RIL_TEXTLINE; //RIL_BLOCK detext table completly and image for PT _IMAGE and table for Pt _TABLE
  if (ri != 0) {
    do {
      const char* word = ri->GetUTF8Text(level);
      float conf = ri->Confidence(level);
      int x1, y1, x2, y2;
      ri->BoundingBox(level, &x1, &y1, &x2, &y2);
      
      //if( ri->BlockType() == PT_FLOWING_IMAGE) // image detection
      // wihtout if  paragraph
      //PT_PULLOUT_TEXT text on corners
      // PT_HEADING_TEXT main heading in images , can be helful in invoice or other forms.
       //if( ri->BlockType() == PT_VERT_LINE  || ri->BlockType() == PT_HORZ_LINE  || ri->BlockType() == PT_TABLE) //tbale detection
      {
        int x1, y1, x2, y2;
        ri->WordFontAttributes(&is_bold,&is_italic,&is_underlined,&is_monospace,&is_serif,&is_smallcaps,&pointsize,&font_id);
        ri->BoundingBox(level, &x1, &y1, &x2, &y2);

        printf("\ntable BoundingBox: %d,%d,%d,%d;\n", x1, y1, x2, y2);
        //cv::rectangle(readImage,cv::Point(x1,y1),cv::Point(x2,y2),cv::Scalar(0,255,0),3,8,0);

        std::cout<<"\n"<<word<<"\n"<<"\n\nIS_BOLD:   "<<is_bold<<"\n\nIS_ITALIC:   "<<is_italic<<"\n\nIS_underline:   "<<is_underlined<<"\n\nIS_MONOSPACE:   "<<is_monospace<<"\n\nIS_serif:   "<<is_serif<<"\n\nIS_smallCaps:   "<<is_smallcaps<<"\n\nIS_pointSize:   "<<pointsize<<"\n\nfontID:   "<<font_id;

        std::cout<<"\n\n@@@@@@@@@@@@@@@@@           "<<ri->WordRecognitionLanguage();
      }
      //printf("word: '%s';  \tconf: %.2f; BoundingBox: %d,%d,%d,%d;\n",
      //         word, conf, x1, y1, x2, y2);
      delete[] word;
    } while (ri->Next(level));
  }

// Destroy used object and release memory
    api->End();
    delete [] outText;
    pixDestroy(&image);

    return 0;
}